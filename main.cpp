#include <phpcpp.h>
#include <iostream>
#include "includes/validator.h"

/**
 *  Switch to C context to ensure that the get_module() function
 *  is callable by C programs (which the Zend engine is)
 */
extern "C" {
    /**
     *  Startup function that is called by the Zend engine
     *  to retrieve all information about the extension
     *  @return void*
     */
    PHPCPP_EXPORT void *get_module() {

        // extension object
        static Php::Extension myExtension("csvvalidator", "1.0");

        // description of the class so that PHP knows
        // which methods are accessible
        Php::Class<CsvValidator> csvValidator("CsvValidator");

        // add methods
        csvValidator.method<&CsvValidator::__construct>("__construct", {
                Php::ByVal("fileName", Php::Type::String),
                Php::ByVal("delimiter", Php::Type::String)
//                Php::ByVal("escape", Php::Type::String)
        });
        csvValidator.method<&CsvValidator::checkColumnCount>("checkColumnCount", {
                Php::ByVal("numberOfColumns", Php::Type::Numeric)
        });
        csvValidator.method<&CsvValidator::setReportLimit>("setReportLimit", {
                Php::ByVal("limit", Php::Type::Numeric)
        });
        csvValidator.method<&CsvValidator::addColumnCheck>("addColumnCheck", {
                Php::ByVal("columnIdx", Php::Type::Numeric),
                Php::ByVal("regex", Php::Type::String)
        });
        csvValidator.method<&CsvValidator::skipFirstLine>("skipFirstLine", {
        		Php::ByVal("skip", Php::Type::Bool)
        });
        csvValidator.method<&CsvValidator::checkQuoteMismatch>("checkQuoteMismatch");
        csvValidator.method<&CsvValidator::forceRegexChecking>("forceRegexChecking");
        csvValidator.method<&CsvValidator::run>("run");

        // add the class to the extension
        myExtension.add(std::move(csvValidator));

        // return the extension
        return myExtension;
    }
}