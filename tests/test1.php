<?php

// Run Simple test
try {
    $validator = new CsvValidator("test.csv", ',');
    $validator->checkColumnCount(3);
    $validator->checkQuoteMismatch();
    $validator->addColumnCheck(0, '([a-zA-Z\ ]+)');
    $validator->forceRegexChecking();
    $validator->setReportLimit(15);
    $report = $validator->run(); // NULL if no errors
    echo "Errors found in first test: ".count($report)."\n";
    var_dump($report);
} catch (Exception $e) {
    echo $e->getMessage();
}