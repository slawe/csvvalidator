<?php
ini_set('memory_limit', '4G');
$before = microtime(true);

// Configure test
$file = "bench_data/12columns/test_5m.csv";
//$file = "bench_data/12columns/test_10m.csv";
$columns = 12;

define("REPORT_LIMIT", 15);

// Run Simple test
echo "Validating file: ".$file."\n";
try {
    $validator = new CsvValidator($file, ',', "\\");
    $validator->checkColumnCount(12); echo "Enable Column Check\n";
    $validator->checkQuoteMismatch(); echo "Enable Quote Mismatch\n";
    $validator->addColumnCheck(0, '([0-9]+)'); echo "Added regex validation to column 0\n";
    $validator->addColumnCheck(1, '([a-zA-Z\ ]+)'); echo "Added regex validation to column 1\n";
    $validator->forceRegexChecking(); echo "Forcing regex checking, this has negative performance impact.\n";
    $validator->setReportLimit(REPORT_LIMIT); echo "Big File Support - Report limit ".REPORT_LIMIT."\n";
//    $validator->addColumnCheck(2, 'rgx1'); echo "Regex column validation\n";
    $report = $validator->run(); // NULL if no errors
    if($report == NULL) {
        echo "\nNo errors found.";
    }else
        echo "\nErrors found in CSV: ".count($report);
    if(count($report) <= REPORT_LIMIT){
        echo "\n";
        var_dump($report);
        echo "\n";
    }
} catch (Exception $e) {
    echo $e->getMessage();
}

$after = microtime(true);
echo "\n".($after-$before) . " seconds to validate\n";
echo "Peak memory usage " . memory_usage() . " for ".fsize($file)." file";




function memory_usage() {
    $mem_usage = memory_get_usage(true);
    $string = '';

    if ($mem_usage < 1024)
        $string .= $mem_usage." bytes";
    elseif ($mem_usage < 1048576)
        $string .= round($mem_usage/1024,2)." kilobytes";
    else
        $string .= round($mem_usage/1048576,2)." megabytes";

    return $string;
}

function fsize($file){

    $mem_usage = filesize($file);
    $string = '';

    if ($mem_usage < 1024)
        $string .= $mem_usage." bytes";
    elseif ($mem_usage < 1048576)
        $string .= round($mem_usage/1024,2)." kilobytes";
    else
        $string .= round($mem_usage/1048576,2)." megabytes";

    return $string;
}
