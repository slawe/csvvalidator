#include "minicsv.h"

#include "validator/config.h"
#include "validator/line.h"
#include "validator/report.h"
#include "validator/line_queue.h"
#include "validator/worker.h"

#include "string.h"
#include "helpers.h"

#include <thread>
#include <algorithm>

using namespace std;

/**
 * @class CsvValidator
 * This class is used for basic CSV structure validation
 */
class CsvValidator: public Php::Base {
private:

	/**
	 * Number of lines that can be read into the memory at once
	 */
	static const int MAX_READ_IN_LINES = 100;

	static const int LINE_QUEUE_PROCESS_LIMIT = 70;

	/**
	 * @var mini::csv::ifstream _is - Input stream
	 */
	mini::csv::ifstream _is;

	/**
	 * @var const char _filename - Path to filename
	 */
	const char *_filename;

	/**
	 * @var validator_config - Validator configuration
	 */
	validator_config _config;

	/**
	 * @var line_queue - List of file lines
	 */
	line_queue<line> _lines;

	/**
	 * @var PHP::Value - Report needs to be an array of associative arrays
	 */
	report _reports;

	/**
	 * @var std::vector<std::thread> - Pool of worker threads
	 */
	std::vector<std::thread> _workers;

	// Methods definitions

	/**
	 * Method that allows you to open file
	 * @param params
	 */
	void _openFile() {

		// Open up CSV
		_is.open(_filename);

		if (!_is.is_open()) {
			stringstream error;
			error << "Unable to open requested file at " << _filename;
			throw Php::Exception(error.str());
		}
		_is.set_delimiter(_config.line_parse_config.delimiter, _config.line_parse_config.unescape_str);
		_is.enable_trim_quote_on_str(_config.line_parse_config.trim_quote_on_str,
									_config.line_parse_config.trim_quote,
									_config.line_parse_config.quote_unescape);
	}

	void _closeFile() {
		_is.close();
	}

	/**
	 * Initialize parallel line validators
	 */
	void _init_workers() {
		// Leave one thread for reading lines
		unsigned short workers_count = std::thread::hardware_concurrency() - 1;
		try {
			for (int i = 0; i < workers_count; i++) {
				_init_worker();
			}
		} catch (...) {
			_config.readIsFinished = true;
			throw("My Error");
		}
	}

	void _init_worker() {
		_workers.push_back(std::thread{worker(_lines, _config, _reports)});
	}

	void _process_lines_until_limit(size_t limit) {
		worker w(_lines, _config, _reports);
		w(LINE_QUEUE_PROCESS_LIMIT);
	}

	void _process_all_lines() {
		_process_lines_until_limit(0);
	}

	void _wait_lines_dequeued() {
		_process_lines_until_limit(LINE_QUEUE_PROCESS_LIMIT);
	}


public:
	CsvValidator() : _filename("") {
		_config.line_parse_config.delimiter = ',';
		_config.readIsFinished = false;
	}

	virtual ~CsvValidator() {
		for (auto &t : _workers) {
			if (t.joinable())
				t.join();
		}
	}

	/**
	 *  PHP "constructor"
	 *  @param params
	 *  @internal params[0] $filename - Path to file.
	 *  @internal params[1] $delimiter - CSV file delimiter, usually comma.
	 *  @internal params[2] $escape - Character used to escape.
	 */
	void __construct(Php::Parameters &params) {
		// Configure parameters
		if (params.empty())
			throw Php::Exception("No arguments specified in constructor.");

		if (params[1].stringValue() == "")
			throw Php::Exception(
					"You need to pass CSV field delimiter as second argument.");

		_config.readIsFinished = false;

//        if (params[2].stringValue() == "")
//            throw Php::Exception("You need to pass escape character as third argument.");

		_filename = (const char *) params[0];
		_config.line_parse_config.delimiter = *(params[1].stringValue().c_str());
//        _config.escape = params[2].stringValue();
		_config.escape = "\\";
	}

	/**
	 * This method enables column count check
	 * @param params
	 * @internal params[0] $numberOfColumns - Integer, number of columns expected in CSV
	 */
	void checkColumnCount(Php::Parameters &params) {
		// Check if parameter is passed
		if (params[0].isNull() || !params[0].isNumeric())
			throw Php::Exception(
					"Column count needs to be passed, and it needs to be an integer value.");

		// Value needs to be greater than zero
		if ((int) params[0] <= 0)
			throw Php::Exception("Column count needs to be greater than zero.");

		_config.columnCount = (int) params[0];
	}

	/**
	 * Enables character pair matching
	 * @param params
	 */
	void checkQuoteMismatch() {
		_config.checkCharMismatch = true;
	}

	/**
	 * Disables validation of the first line (header)
	 */
	void skipFirstLine(Php::Parameters &params) {
		_config.skipFirstLine = (bool) params[0];
	}

	/**
	 * Adds new column check to array.
	 * @param params
	 * @internal params[0] $columnIdx - Integer, index of column we're adding validation for
	 * @internal params[1] $regex - String, regular expression that we'll use to validate column
	 */
	void addColumnCheck(Php::Parameters &params) {
		// Check if parameter is passed
		if (params[0].isNull() || !params[0].isNumeric()) {
			throw Php::Exception(
					"Column index needs to be passed, and it needs to be an integer value.");
		}

		// Value needs to be greater than zero
		if ((int) params[0] < 0)
			throw Php::Exception("Column index cannot be negative.");

		if (params[1].isNull())
			throw Php::Exception("Regular expression is not passed.");

		_config.columnsChecks[(int) params[0]].push_back(
			std::move(regex_rule{params[1].stringValue()})
		);
	}

	/**
	 * This method enables support for very large files.
	 * In this case, if file has more than $limit errors reported already,
	 * we won't be adding any new
	 * @param params
	 * @internal params[0] $limit - Limit to how many errors reported
	 */
	void setReportLimit(Php::Parameters &params) {

		// Check if parameter is passed
		if (params[0].isNull() || !params[0].isNumeric())
			throw Php::Exception(
					"Limit needs to be passed, and it needs to be an integer value.");

		// Value needs to be greater than zero
		if ((int) params[0] <= 0)
			throw Php::Exception(
					"Limit needs to be greater than zero, default value is zero.");

		_reports.setLimit((int) params[0]);
	}

	/**
	 * Forces regex checking on cells.
	 * Keep in mind, this could have negative performance impact!
	 */
	void forceRegexChecking() {
		_config.forceRegexCheck = true;
	}

	/**
	 * This method starts validation process.
	 */
	Php::Value run() {
		_openFile();

		// Run background line processors
		_init_workers();

		if (_config.skipFirstLine) {
			// Skip header
			_is.skip_line();
		}
		// Read the rest of the file and periodically run processing
		while (_is.read_line() && !_reports.full()) {
			if (_lines.size() > MAX_READ_IN_LINES) {
				_wait_lines_dequeued();
			}
			_lines.push(line{_is.get_line_number(), _is.get_line(), _config.line_parse_config});
		}

		_closeFile();

		// Indicate that file reading is completed
		_config.readIsFinished = true;

		// Process the rest of lines from queue
		_process_all_lines();

		for (auto &t : _workers) {
			t.join();
		}
		_config.readIsFinished = false;

		return _reports.get();
	}
};
