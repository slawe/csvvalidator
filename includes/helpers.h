#ifndef INCLUDES_VALIDATOR_HELPERS_H_
#define INCLUDES_VALIDATOR_HELPERS_H_

#include <string>

using namespace std;

/**
 * Helper method to replace strings in place
 * @param subject
 * @param search
 * @param replace
 */
void replace_string_in_place(std::string &subject, const std::string &search,
                             const std::string &replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
        subject.replace(pos, search.length(), replace);
        pos += replace.length();
    }
}

/**
 * Helper method to count character occurrences
 * @param s
 * @param character
 * @return
 */
int count_occurences(std::string s, char character) {
    int count = 0;

    for (unsigned int i = 0; i < s.size(); i++)
        if (s[i] == character) count++;

    return count;
}

inline std::string trim_right(const std::string &str, const std::string &trimChars) {
    std::string result = "";
    size_t endpos = str.find_last_not_of(trimChars);
    if (std::string::npos != endpos) {
        result = str.substr(0, endpos + 1);
    } else
        result = str;

    return result;
}

inline std::string trim_left(const std::string &str, const std::string &trimChars) {
    std::string result = "";

    size_t startpos = str.find_first_not_of(trimChars);
    if (std::string::npos != startpos) {
        result = str.substr(startpos);
    } else
        result = str;

    return result;
}

inline std::string trim(const std::string &str, const std::string &trimChars) {
    return trim_left(trim_right(str, trimChars), trimChars);
}

#endif // INCLUDES_VALIDATOR_HELPERS_H_
