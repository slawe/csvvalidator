#ifndef INCLUDES_VALIDATOR_LINE_H_
#define INCLUDES_VALIDATOR_LINE_H_

#include "config.h"
#include "../helpers.h"

#include <string>

struct line {
	std::string content;

	/**
	 * Default constructor
	 */
	line() = delete;

	line(const line &other) : content(other.content), number(other.get_number()), pos(0), token_num(0), _parse_config(other.get_config()) {}


	line(std::string content, const line_parse_config_t &config) :
			content(content), number(0), pos(0), token_num(0), _parse_config(std::ref(config)) {}

	line(size_t number, std::string content, const line_parse_config_t &config) :
			content(content), number(number), pos(0), token_num(0), _parse_config(std::ref(config)) {}

	std::string get_delimited_str() {
		std::string str = "";
		char ch = '\0';
		bool within_quote = false;
		do {
			if (pos >= this->content.size()) {
				this->content = "";

				++token_num;
				return unescape(str);
			}

			ch = this->content[pos];
			if (_parse_config.trim_quote_on_str) {
				if (!within_quote && ch == _parse_config.trim_quote
						&& ((pos > 0
								&& this->content[pos - 1]
										== _parse_config.delimiter) || pos == 0)) {
					within_quote = true;
				}
				// @NOTE Added escaped quote check before closing
				else if (within_quote && ch == _parse_config.trim_quote
						&& _parse_config.escape_char[0]
								!= this->content[pos - 1]) {
					within_quote = false;
				}
			}

			++(pos);

			if (ch == _parse_config.delimiter && !within_quote)
				break;
			if (ch == '\r' || ch == '\n')
				break;

			str += ch;
		} while (true);

		++token_num;
		return unescape(str, true);
	}

	size_t num_of_delimiter() const {
		if (_parse_config.delimiter == '\0')
			return 0;

		size_t cnt = 0;
		if (_parse_config.trim_quote_on_str) {
			bool inside_quote = false;
			for (size_t i = 0; i < content.size(); ++i) {
				if (content[i] == _parse_config.trim_quote
						&& content[i - 1] != _parse_config.escape_char[0])
					inside_quote = !inside_quote;

				if (!inside_quote) {
					if (content[i] == _parse_config.delimiter)
						++cnt;
				}
			}
		} else {
			cnt = std::count(content.begin(), content.end(),
					_parse_config.delimiter);
		}
		return cnt;
	}

	std::string get_rest_of_line() const {
		return content.substr(pos);
	}

	size_t get_number() const {
		return number;
	}

	line & operator=(line&& _line) {
		content = std::move(_line.content);
		number = std::move(_line.get_number());
		const_cast<line_parse_config_t &>(_parse_config) = std::move(_line.get_config());
		pos = 0;
		token_num = 0;
		return *this;
	}

	const line_parse_config_t & get_config() const {
		return _parse_config;
	}

private:
	size_t number;
	size_t pos;
	size_t token_num;
	const line_parse_config_t &_parse_config;

	std::string unescape(std::string &src,
	bool replaceEscapedString = false) {
		if (_parse_config.unescape_str.empty())
			replace(src, _parse_config.unescape_str, std::string(1, _parse_config.delimiter));

		if (_parse_config.trim_quote_on_str) {
			std::string s = trim(src, std::string(1, _parse_config.trim_quote));
			return (replaceEscapedString) ?
					replace(s, _parse_config.quote_unescape,
							std::string(1, _parse_config.trim_quote)) :
					s;
		}

		return src;
	}

	inline const std::string &replace(std::string &src,
			const std::string &to_find, const std::string &to_replace) {
		size_t pos = 0;
		while (std::string::npos != pos) {
			pos = src.find(to_find, pos);

			if (std::string::npos != pos) {
				src.erase(pos, to_find.size());
				src.insert(pos, to_replace);
				pos += to_replace.size();
			}
		}

		return src;
	}
};

#endif /* INCLUDES_VALIDATOR_LINE_H_ */
