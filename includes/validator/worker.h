#ifndef INCLUDES_VALIDATOR_LINEQUEUE_H_
#define INCLUDES_VALIDATOR_LINEQUEUE_H_

#include "config.h"
#include "line_queue.h"
#include "../helpers.h"
#include "../minicsv.h"
#include "line.h"

#include <string>
#include <regex>
#include <unordered_map>

class worker {
public:

	worker(	line_queue<line> &lines, const validator_config &config, report &report) :
			_lines(lines), _config(config), _report(report) {}

	void operator() () const
	{
		while ((!_config.readIsFinished || !_lines.empty()) && !_report.full()) {
			_process_next_line();
		}
	}

	/**
	 * Process lines from line queue until process_limit lines are left
	 *
	 * @param until_lines_in_queue
	 */
	void operator()(size_t process_limit) const {
		while ((_lines.size() > process_limit || !_lines.empty()) && !_report.full()) {
			_process_next_line();
		}
	}

private:
	line_queue<line> &_lines;
	const validator_config &_config;
	report &_report;

	/**
	 * Reads and process next line from lines queue
	 */
	void _process_next_line() const {
		line current_line { "NONE", _config.line_parse_config };
		if (_lines.try_pop(current_line)) {

			// Check column count
			bool columnCheckReportAdded = _checkColumnCount(_config,
					current_line);

			// Check quotation mark pair mismatch
			bool quoteMismatchCheckReportAdded = _checkQuoteMismatch(_config,
					current_line);

			//          If two most basic checks didn't pass (reports added), it'd be futile to do any of the advanced ones
			//          Check if there are any regexes added
			//          Same goes if there aren't any checks added.
			//          We have to do regex check if _forceRegexCheck is set to true, even if 2 tests passed

			if ((_config.columnsChecks.size() == 0) || ((columnCheckReportAdded || quoteMismatchCheckReportAdded) && !_config.forceRegexCheck)) {
				return;
			}

			_checkRegexValidation(_config, current_line);
		}
	}

	/**
	 * This method is used to check if number of columns in row matches number of columns
	 * @return Php::Object lineReport
	 */
	bool _checkColumnCount(const validator_config & config,
								  const line & _line) const
	{
		bool r = false;

		// Columns = delimiters + 1
		// test,test1,test
		// Above row has 2 delimiters => 3 columns
		int columnsFound = (int) _line.num_of_delimiter() + 1;

		// Check if delimiter count matches requirement
		if (columnsFound != config.columnCount) {
			std::stringstream error;
			error << "Column count of ";
			error << columnsFound;
			error << " does not match requirement ( ";
			error << config.columnCount << " columns )";
			r = _report.generateAndAdd(_line, std::move(error.str()));
		};

		return r;
	}


	/**
	 * Check if character has it's matching pair.
	 * For example, if opening quote has matching ending quote
	 *
	 * @return Php::Object lineReport
	 */
	bool _checkQuoteMismatch(const validator_config & config,
			const line & _line) const
	{
		bool r = false;
		std::string line = _line.get_rest_of_line();

		// Clear escaped quotes for purpose of this check
		std::string matchStr = config.escape + "\"";
		std::string repStr = " ";

		//        cout << "LINE: " << line <<endl;
		replace_string_in_place(line, matchStr, repStr);
		int quotationsCount = count_occurences(line, '"');
		//        cout << "ESC: " << matchStr << endl;
		//        cout << "REP: " << repStr << endl;
		//        cout << "LINE AFTER: " << line <<endl;
		//        cout << "QCNT: " << quotationsCount << " " << (quotationsCount%2) << endl;

		if ((quotationsCount % 2) != 0) {
			r = _report.generateAndAdd(_line, "Unpaired quotation mark.");
		}

		return r;
	}

	/**
	 * Checks if row's columns match given regex-es
	 * @return list - This method can generate multiple reports for single line
	 */
	void _checkRegexValidation(const validator_config & config, line &_line) const
	{
		for (unsigned columnIdx = 0;
				columnIdx <= _line.num_of_delimiter() && !_report.full();
				columnIdx++)
		{
			std::string value = _line.get_delimited_str();

			try {
				for (auto & re_rule : _config.columnsChecks.at(columnIdx)) {
					if (_report.full())
						break;

					if (!std::regex_match(value, re_rule.re())) {
						_report.generateAndAdd(_line, "Regex '" +
								re_rule.str() +
								"' does not match string in column " +
								std::to_string(columnIdx) +
								": '" + value + "'.");
					}
				}
			} catch (std::out_of_range & e) { }
		}
	}
};

#endif /* INCLUDES_VALIDATOR_LINEQUEUE_H_ */
