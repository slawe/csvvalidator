#ifndef INCLUDES_VALIDATOR_REPORT_H_
#define INCLUDES_VALIDATOR_REPORT_H_

#include <mutex>
#include <unordered_map>

typedef Php::Object plain_report_item;

class report {
public:

	report() : _limit(100) {}
	report(int limit) : _limit(limit) {}

	report(const report&) = delete;            // disable copying
	report& operator=(const report&) = delete; // disable assignment

	/**
	 * Helper method to add report to reports list
	 * @param object
	 * @return bool - True if report is added, false if not
	 */
	bool add(plain_report_item line_report) {
		std::unique_lock<std::mutex> mlock{_mutex};

		// If there's nothing to report, skip
		if (!line_report["line"])
			return false;

		// If we're over report limit, skip adding
		if (full())
			return false;

		_rpt[_rpt.size()] = line_report;
		return true;
	}

	const Php::Value & get() {
		return _rpt;
	}

	void setLimit(int limit) {
		_limit = limit;
	}

	bool full() const {
		return _limit == _rpt.size();
	}

	/**
	 * Helper method to generate report
	 * @param line - Line description
	 * @param std::string - Error message
	 * @return
	 */
	bool generateAndAdd(const line & line, const std::string & error) {
		std::unique_lock<std::mutex> mlock{_gen_mutex};
		plain_report_item lineReport;
	    int num = line.get_number();
	    lineReport["line"] = num;
	    lineReport["line_content"] = line.content;
	    lineReport["message"] = error;
	    return add(lineReport);
	}

private:
	Php::Value _rpt;
	std::mutex _mutex;
	std::mutex _gen_mutex;
	int _limit;
};

#endif /* INCLUDES_VALIDATOR_REPORT_H_ */
