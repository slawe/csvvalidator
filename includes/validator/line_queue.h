#ifndef INCLUDES_VALIDATOR_LINE_QUEUE_H_
#define INCLUDES_VALIDATOR_LINE_QUEUE_H_

#include <queue>
#include <mutex>
#include <condition_variable>
#include <algorithm>

template<typename T>
class line_queue {
public:

	T pop() {
		std::unique_lock<std::mutex> mlock(_mutex);
		while (_queue.empty()) {
			_condition.wait(mlock);
		}
		auto val = _queue.front();
		_queue.pop();
		return val;
	}

	void pop(T& item) {
		std::unique_lock<std::mutex> mlock(_mutex);
		while (_queue.empty()) {
			_condition.wait(mlock);
		}
		item = _queue.front();
		_queue.pop();
	}

	bool try_pop(T& item)
	{
		std::unique_lock<std::mutex> mlock(_mutex);
		if(_queue.empty())
			return false;

		item = std::move(_queue.front());
		_queue.pop_front();

		return true;
	}

	void push(const T&& item) {
		std::unique_lock<std::mutex> mlock(_mutex);
		_queue.push_back(std::move(item));
		mlock.unlock();
		_condition.notify_one();
	}

	std::size_t size() {
		std::unique_lock<std::mutex> mlock(_mutex);
		return _queue.size();
	}

	bool empty() const {
		return _queue.empty();
	}

	line_queue() = default;

	line_queue(const line_queue&) = delete;            // disable copying
	line_queue& operator=(const line_queue&) = delete; // disable assignment

private:

	std::deque<T> _queue;
	std::mutex _mutex;
	std::condition_variable _condition;
};

#endif /* INCLUDES_VALIDATOR_LINE_QUEUE_H_ */
