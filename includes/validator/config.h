#ifndef INCLUDES_VALIDATOR_CONFIG_H_
#define INCLUDES_VALIDATOR_CONFIG_H_

#include <string>
#include <list>
#include <atomic>
#include <unordered_map>
#include <regex>

struct regex_rule {
	regex_rule(std::string str) : _str(str), _re(std::regex(str, std::regex::optimize)) {}

	const std::string & str() const {return _str;}
	const std::regex & re() const {return _re;}

private:
	std::string _str;
	std::regex _re;
};

typedef struct {
	/**
	 * @var char delimiter - CSV Field delimiter
	 */
	char delimiter;

	/**
	 * @var std::string - Quote escape sequence
	 */
	std::string quote_unescape = "\\\"";

	/**
	 * @var std::string - <minicsv related config>
	 */
	std::string unescape_str = "##";

	std::string escape_char = "\\";

	/**
	 * @var bool - Flag which determines if
	 */
	bool trim_quote_on_str = true;

	/**
	 * Trim
	 */
	char trim_quote = '\"';
} line_parse_config_t;

typedef std::unordered_map<int, std::vector<regex_rule>> column_check_regex_list_t;

struct validator_config {
	/**
	 * @var std::string - Character used to escape values
	 */
	std::string escape;

	/**
	 * @var int - Number of columns expected in CSV. If set to 0, no column count validation will be performed.
	 */
	int columnCount = 0;

	/**
	 * @var bool - If true, character pair mismatch will be checked for specific character
	 */
	bool checkCharMismatch = false;

	/**
	 * @var column_check_regex_list_t - List of column check regex
	 */
	column_check_regex_list_t columnsChecks;

	/**
	 * @var bool - If true, regex checking will be performed regardless if basic tests failed.
	 */
	bool forceRegexCheck = false;

	/**
	 * @var bool Disable/Enable validation of the csv header
	 */
	bool skipFirstLine = true;

	/**
	 * @var line_parse_config - Bundle of configurations related to line parsing
	 */
	line_parse_config_t line_parse_config;

	/**
	 * @var bool - Indicates that file reading has been finished.
	 */
	std::atomic<bool> readIsFinished;
};

#endif /* INCLUDES_VALIDATOR_CONFIG_H_ */
